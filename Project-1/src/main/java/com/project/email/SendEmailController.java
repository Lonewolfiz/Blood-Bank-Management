package com.project.email;

import java.io.IOException;
import java.net.http.HttpResponse;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.AddressException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.availability.ReadinessState;
import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import com.project.DAO.UserDAO;
import com.project.model.Login;
import com.project.model.User;

@Controller

public class SendEmailController {
	public int otp;
	@Autowired
	SendingEmailApplication sendingEmailApplication;

	@Autowired
	private UserDAO userDAO;

	@Autowired
	private User user;

	@RequestMapping(value = "/sendemail", method = RequestMethod.GET)
	public String send() {
		return "email";
	}

	@RequestMapping(value = "/sendemail", method = RequestMethod.POST)
	public String mailsent(@RequestParam("email") String email, HttpServletRequest request,
			HttpServletResponse response) throws MessagingException, IOException, javax.mail.MessagingException {
		HttpSession session = request.getSession();
		session.setAttribute("email", email);
		sendingEmailApplication.sendEmail(email);
		otp = sendingEmailApplication.otpreturn();

		return "sent-success";
	}

	@RequestMapping(value = "/page", method = RequestMethod.GET)

	public String configure() {

		return "enter-otp";
	}

	@RequestMapping(value = "/page", method = RequestMethod.POST)

	public String configured(@RequestParam("otp") int checker, Model m) {

		if (checker == otp) {
			return "success";
		} else {
			m.addAttribute("error", "entered otp is incorrect");
			return "enter-otp";
		}

	}

	@RequestMapping(value = "/changePassword", method = RequestMethod.GET)

	public String changePassword() {

		return "success";
	}

	@RequestMapping(value = "/changePassword", method = RequestMethod.POST)

	public String changePassword1(@RequestParam("password") String password, HttpServletRequest request,
			HttpServletResponse response) {
		HttpSession session = request.getSession();
		int result = userDAO.changePassword(password, (String) session.getAttribute("email"));
		return "redirect:/login";
	}

	@RequestMapping("/send-notify-message")
	public String notify(@RequestParam("name") String username, Model model) {

		sendingEmailApplication.sendnotifymailEmail(username);

		// alert("successful");
		return "info";
	}
	

}
