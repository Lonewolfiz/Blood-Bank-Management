package com.project.email;

import java.io.File;
import java.io.IOException;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.Random;

@SpringBootApplication
public class SendingEmailApplication {

	@Autowired
	private JavaMailSender javaMailSender;

	

	public int y;

	void sendEmail(String email) {

		SimpleMailMessage msg = new SimpleMailMessage();
		msg.setTo(email);
		Random rand = new Random();
		y = rand.nextInt(10000);

		msg.setSubject("Oops!Forgot your passsword");
		msg.setText("Here is the OTP \n to get back you in the track \n otp is \n " + y);

		javaMailSender.send(msg);

		System.out.println("---------" + y);

	}

	void sendnotifymailEmail(String email) {

		SimpleMailMessage msg = new SimpleMailMessage();
		msg.setTo(email);
		msg.setSubject("Emergency-It's Time to Help!");
		msg.setText(" A person is in need of your help!!! ");
		javaMailSender.send(msg);
	}

	public int otpreturn() {
		return y;
	}

}