package com.project.DAO;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.Mapping;

///import com.project.model.Admin;
import com.project.model.Login;
import com.project.model.User;

@Component
public class UserDAOImpl implements UserDAO {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	public int insertUser(User U) {
		String query = "insert into User values(?,?,?,?,?,?,?,?,?,?,?,?)";
		Object[] args = new Object[] { U.getName(), U.getUsername(), U.getMobile(), U.getBlood(), U.getState(),
				U.getCity(), U.getGender(), U.getPassword(), U.getDateOfBirth(), U.getDateOfLastDonation(), "ROLE_USER",
				1 };
		int out = jdbcTemplate.update(query, args);

		return out;

	}

	public List<User> getUsersByBlood(String blood, String mail) {

		List<User> uList = new ArrayList<User>();
		System.out.println(mail);
		String query = "select * from user where blood=? and username<>? and ((TIMESTAMPDIFF(MONTH,dateOfLastDonation, SYSDATE()))>6 or dateOfLastDonation is null );";
		List<Map<String, Object>> userRows = jdbcTemplate.queryForList(query, blood, mail);

		for (Map<String, Object> i : userRows) {
			User user = new User();

			user.setName((String) i.get("name"));
			user.setUsername((String) i.get("username"));
			user.setMobile((String) i.get("mobile"));
			user.setBlood((String) i.get("blood"));
			user.setState((String) i.get("state"));
			user.setCity((String) i.get("city"));
			user.setGender((String) i.get("gender"));
			uList.add(user);
			System.out.println(user);
		}

		return uList;

	}

	public List<User> getUsersByBlood(String blood, String state, String mail) {

		List<User> uList = new ArrayList<User>();
		String query = "select * from User where blood=?  and state=? and username<>? and  ((TIMESTAMPDIFF(MONTH,dateOfLastDonation, SYSDATE()))>6 or dateOfLastDonation IS NULL); ";
		List<Map<String, Object>> userRows = jdbcTemplate.queryForList(query, blood, state, mail);

		for (Map<String, Object> i : userRows) {
			User user = new User();

			user.setName((String) i.get("name"));
			user.setUsername((String) i.get("username"));
			user.setMobile((String) i.get("mobile"));
			user.setBlood((String) i.get("blood"));
			user.setState((String) i.get("state"));
			user.setCity((String) i.get("city"));
			user.setGender((String) i.get("gender"));
			uList.add(user);
			System.out.println(user);
		}

		return uList;

	}

	@Override
	public int updateUser(User U) {
		String query = "update user set name=?,mobile=?,blood=?,state=?,city=?,gender=?,dateOfBirth=?,dateOfLastDonation=? where username=?";
		Object[] args = new Object[] { U.getName(), U.getMobile(), U.getBlood(), U.getState(), U.getCity(),
				U.getGender(), U.getDateOfBirth(), U.getDateOfLastDonation(), U.getUsername() };

		int out = jdbcTemplate.update(query, args);
		return out;
	}

	@Override
	public User validateUser(Login login) {
		String sql = "select * from user where username='" + login.getUsername() + "' and password='"
				+ login.getPassword() + "'";
		List<User> user = jdbcTemplate.query(sql, new UserMapper());

		return user.size() > 0 ? user.get(0) : null;
	}
	

	@Override
	public User getUserByMail(String username) { // for profile page

		String query = "select * from user where username=?";

		User user = jdbcTemplate.queryForObject(query, new Object[] { username }, new RowMapper<User>() {

			public User mapRow(ResultSet rs, int rowNum) throws SQLException {
				User user = new User();

				user.setName(rs.getString(1));
				user.setUsername(rs.getString(2));

				user.setMobile(rs.getString(3));
				user.setBlood(rs.getString(4));
				user.setState(rs.getString(5));
				user.setCity(rs.getString(6));
				user.setGender(rs.getString(7));
				user.setDateOfBirth(rs.getDate(9));
				user.setDateOfLastDonation(rs.getDate(10));

				return user;
			}

		});
		return user;

	}

	@Override
	public int changePassword(String password, String mail) {
		String query = "update user set password=? where username=?";
		int out = jdbcTemplate.update(query, password, mail);
		return out;

	}

	@Override
	public List<User> getSuggestionBlood(String blood, String state) {
		List<User> uList = new ArrayList<User>();
		String query;
		if (blood == "A+" || blood == "A1+") {
			query = "select * from User where blood in ('A-','O+','O-')  and state=state and ((TIMESTAMPDIFF(MONTH,dateOfLastDonation, SYSDATE()))>6 or dateOfLastDonation IS NULL); ";
		} else if (blood == "B+") {
			query = "select * from User where blood in ('B-','AB+','AB-')  and state=state and ((TIMESTAMPDIFF(MONTH,dateOfLastDonation, SYSDATE()))>6 or dateOfLastDonation IS NULL); ";
		} else if (blood == "A-") {
			query = "select * from User where blood in ('O-') and state=state and ((TIMESTAMPDIFF(MONTH,dateOfLastDonation, SYSDATE()))>6 or dateOfLastDonation IS NULL); ";
		} else if (blood == "AB+") {
			query = "select * from User where blood in ('A-','O+','O-','B-','AB+','AB-','B+','A1+','A1-') and state=state and ((TIMESTAMPDIFF(MONTH,dateOfLastDonation, SYSDATE()))>6 or dateOfLastDonation IS NULL); ";
		} else if (blood == "AB-") {
			query = "select * from User where blood in ('A-','B-','O-') and state=state  and ((TIMESTAMPDIFF(MONTH,dateOfLastDonation, SYSDATE()))>6 or dateOfLastDonation IS NULL); ";
		} else if (blood == "O+" || blood == "B-" || blood == "O-") {
			query = "select * from User where blood in ('O-') and state=state  and ((TIMESTAMPDIFF(MONTH,dateOfLastDonation, SYSDATE()))>6 or dateOfLastDonation IS NULL); ";
		} else {
			query = "select * from User where blood not in ('A-','O+','O-','B-','AB+','AB-','B+','A1+','A1-')  and ((TIMESTAMPDIFF(MONTH,dateOfLastDonation, SYSDATE()))>6 or dateOfLastDonation IS NULL); ";
		}
		List<Map<String, Object>> userRows = jdbcTemplate.queryForList(query);

		for (Map<String, Object> i : userRows) {
			User user = new User();

			user.setName((String) i.get("name"));
			user.setUsername((String) i.get("username"));
			user.setMobile((String) i.get("mobile"));
			user.setBlood((String) i.get("blood"));
			user.setState((String) i.get("state"));
			user.setCity((String) i.get("city"));
			user.setGender((String) i.get("gender"));

			uList.add(user);
			System.out.println(user);
		}

		return uList;
	}

	@Override
	public List<User> getSuggestionBlood(String blood) {
		List<User> uList = new ArrayList<User>();
		String query;
		if (blood == "A+" || blood == "A1+") {
			query = "select * from User where blood in ('A-','O+','O-')  and ((TIMESTAMPDIFF(MONTH,dateOfLastDonation, SYSDATE()))>6 or dateOfLastDonation IS NULL); ";
		} else if (blood == "B+") {
			query = "select * from User where blood in ('B-','AB+','AB-')  and ((TIMESTAMPDIFF(MONTH,dateOfLastDonation, SYSDATE()))>6 or dateOfLastDonation IS NULL); ";
		} else if (blood == "A-") {
			query = "select * from User where blood in ('O-')  and ((TIMESTAMPDIFF(MONTH,dateOfLastDonation, SYSDATE()))>6 or dateOfLastDonation IS NULL); ";
		} else if (blood == "AB+") {
			query = "select * from User where blood in ('A-','O+','O-','B-','AB+','AB-','B+','A1+','A1-')  and ((TIMESTAMPDIFF(MONTH,dateOfLastDonation, SYSDATE()))>6 or dateOfLastDonation IS NULL); ";
		} else if (blood == "AB-") {
			query = "select * from User where blood in ('A-','B-','O-')  and ((TIMESTAMPDIFF(MONTH,dateOfLastDonation, SYSDATE()))>6 or dateOfLastDonation IS NULL); ";
		} else if (blood == "O+" || blood == "B-" || blood == "O-") {
			query = "select * from User where blood in ('O-')  and ((TIMESTAMPDIFF(MONTH,dateOfLastDonation, SYSDATE()))>6 or dateOfLastDonation IS NULL); ";
		} else {
			query = "select * from User where blood not in ('A-','O+','O-','B-','AB+','AB-','B+','A1+','A1-')  and ((TIMESTAMPDIFF(MONTH,dateOfLastDonation, SYSDATE()))>6 or dateOfLastDonation IS NULL); ";
		}
		List<Map<String, Object>> userRows = jdbcTemplate.queryForList(query);

		for (Map<String, Object> i : userRows) {
			User user = new User();

			user.setName((String) i.get("name"));
			user.setUsername((String) i.get("username"));
			user.setMobile((String) i.get("mobile"));
			user.setBlood((String) i.get("blood"));
			user.setState((String) i.get("state"));
			user.setCity((String) i.get("city"));
			user.setGender((String) i.get("gender"));

			uList.add(user);
			System.out.println(user);
		}

		return uList;
	}

}

class UserMapper implements RowMapper<User> {

	@Override
	public User mapRow(ResultSet rs, int arg1) throws SQLException {
		User user = new User();

		user.setName(rs.getString("name"));
		user.setUsername(rs.getString("username"));
		user.setMobile(rs.getString("mobile"));
		user.setBlood(rs.getString("blood"));
		user.setState(rs.getString("state"));
		user.setCity(rs.getString("city"));
		user.setGender(rs.getString("gender"));
		user.setPassword(rs.getString("password"));
		user.setDateOfBirth(rs.getDate("dateOfBirth"));
		user.setDateOfLastDonation(rs.getDate("dateOfLastDonation"));

		return user;
	}

}
