package com.project.DAO;

import java.util.List;

import org.springframework.stereotype.Component;

import com.project.model.User;
//import com.project.model.Admin;
import com.project.model.Login;
import com.project.model.User;

@Component
public interface UserDAO {

	public int insertUser(User U);

	public List<User> getUsersByBlood(String blood,String state,String mail);
	
	public List<User> getUsersByBlood(String blood,String mail);
	public List<User> getSuggestionBlood(String blood,String state);
	public List<User> getSuggestionBlood(String blood);
	public int updateUser(User U);
    public int changePassword(String password,String mail);
	User validateUser(Login login);
	

	public User getUserByMail(String username);

}
