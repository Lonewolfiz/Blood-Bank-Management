 package com.project.security;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.web.csrf.CookieCsrfTokenRepository;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;



@EnableWebSecurity
@Configuration

public class ApplicationSecurityConfig extends WebSecurityConfigurerAdapter{
	
	@Autowired
    private DataSource dataSource;
	
	 @Autowired
	 private EmployeeAuthenticationSuccessHandler successHandler;
	
     
    @Autowired
    public void configAuthentication(AuthenticationManagerBuilder auth) throws Exception {
        auth.jdbcAuthentication()
            .dataSource(dataSource)
            .usersByUsernameQuery("select username, password, enabled from user where username=?")
            .authoritiesByUsernameQuery("select username, role from user where username=?").passwordEncoder(passwordEncoder());
        
    }
	/*
	 * @Autowired public void configAuthentication1(AuthenticationManagerBuilder
	 * auth) throws Exception { auth.jdbcAuthentication() .dataSource(dataSource)
	 * .usersByUsernameQuery("select username, password, enabled from admin where username=?"
	 * )
	 * .authoritiesByUsernameQuery("select username, role from admin where username=?"
	 * ).passwordEncoder(passwordEncoder());
	 * 
	 * }
	 */
 
    @Override
    protected void configure(HttpSecurity http) throws Exception {
    	http
    	.csrf().disable()  //generate unique token for each user
        .authorizeRequests()
        .antMatchers("/home").hasAnyRole("USER")
        .antMatchers("/sendemail").permitAll()
        
        .antMatchers("/register/**").permitAll()
        .antMatchers("/submitForm/**").permitAll()
       
        .antMatchers("/css/**").permitAll()
		.antMatchers("/image/**").permitAll()
		
		.antMatchers("/js/**").permitAll()
		.antMatchers("/homepage","/images/**","/registrationpage","/changePassword","/page","/infohome","/Homepage").permitAll()
	
		
       
        .antMatchers("/").permitAll()
         
           .anyRequest().authenticated()
           .and()
        .formLogin()
          
           .loginPage("/login")
           
         
          
           .successHandler(successHandler)
           .permitAll()
           .and()
           .logout()
           .permitAll();
    	
  }
   
    @Bean
	public static NoOpPasswordEncoder passwordEncoder() {
	  return (NoOpPasswordEncoder) NoOpPasswordEncoder.getInstance();
	}
	
	
}
