package com.project.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import com.project.DAO.UserDAO;
import com.project.model.User;

@Controller
public class ApplicationController {

	@Autowired
	private UserDAO userDao;

	@RequestMapping("/search")
	public String search(@RequestParam("blood") String blood, @RequestParam("state") String state, Model model,
			HttpServletRequest request, HttpServletResponse response, Authentication authentication) {
		HttpSession session = request.getSession();
		session.setAttribute("blood", blood);
		session.setAttribute("state", state);
		String username = authentication.getName();
		List<User> user;

		if (state.equals("null")) {

			user = userDao.getUsersByBlood(blood, username);

		} else {

			user = userDao.getUsersByBlood(blood, state, username);
		}
		if (user.size() == 0) {
			model.addAttribute("record", "No donors found in this Category!!!");
			return "getByBloodGroup";
		} else {
			model.addAttribute("user", user);
			return "showByBloodGroup";
		}

	}

	@RequestMapping(value = "/about")
	public String displayUser() {

		return "about";

	}

	@RequestMapping("/home")
	public String infoHome(Model model, HttpSession session) {
		model.addAttribute("username", (String) session.getAttribute("usermail"));
		return "home";
	}

	@RequestMapping(value = "/saveupdate", method = RequestMethod.POST)
	public String saveupdate(@ModelAttribute("uuser") User U, Model m) {
		System.out.println("Save User");
		int result = userDao.updateUser(U);
		if (result > 0) {

			m.addAttribute("message", "Updated Successfully");
		} else {

			m.addAttribute("message", "Not Updated Successfully");
		}

		return "about";

	}

	@RequestMapping(value = "/view", method = RequestMethod.GET)
	public String viewbyblood() {

		return "getByBloodGroup";
	}

	@RequestMapping(value = "/about", method = RequestMethod.GET)
	public String about(Model m, Authentication authentication) {
		String username = authentication.getName();
		User user = userDao.getUserByMail(username);
		m.addAttribute("uuser", user);

		return "about";
	}

	@RequestMapping("/registrationpage")
	public String returntoRegistration() {
		return "registrationpage";
	}

	@RequestMapping("/Homepage")
	public String returntoHome() {
		return "Homepage";
	}

	@RequestMapping("/getByBloodGroup")
	public String returntoGetByBloodGroup(Model model, HttpSession session) {
		model.addAttribute("username", (String) session.getAttribute("usermail"));
		return "getByBloodGroup";
	}

	@RequestMapping("/suggestion")
	public String suggesstion(HttpSession session, Model model) {
		List<User> user;
		String state = (String) session.getAttribute("state");
		if (state.equals("null")) {

			user = userDao.getSuggestionBlood((String) session.getAttribute("blood"));

		} else {
			user = userDao.getSuggestionBlood((String) session.getAttribute("blood"),
					(String) session.getAttribute("state"));

		}
		if (user.size() == 0) {
			model.addAttribute("suggestionrecord", "No donors found in this Category");
			return "getByBloodGroup";
		} else {
			model.addAttribute("user", user);
			return "suggesstion";
		}

	}

}