package com.project.controller;

import javax.servlet.http.HttpSession;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class GenericController {

	@RequestMapping("/info")
	public String infoPage(Model model, HttpSession session) {
		model.addAttribute("mail", (String) session.getAttribute("usermail"));
		return "info";
	}

	@RequestMapping("/infohome")
	public String infoHomePage(Model model, HttpSession session) {
		model.addAttribute("mail", (String) session.getAttribute("usermail"));
		return "infohome";
	}

}