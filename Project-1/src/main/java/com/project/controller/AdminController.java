package com.project.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.project.DAO.UserDAO;

import com.project.model.User;

@Controller
public class AdminController {
	@Autowired
	private UserDAO userDao;

	@Autowired
	private User user;

	@RequestMapping("/")
	public String homePage() {
		return "Homepage";
	}

	

	
	@RequestMapping("/register")
	public String newUser(ModelMap map) {

		map.addAttribute("registration", user);
		map.addAttribute("addMsg", "");
		return "registrationpage";
	}

	@RequestMapping("/submitForm")
	public String saveUser(@ModelAttribute("registration") User U, Model m) {
		int result = userDao.insertUser(U);
		return "redirect:/login";
	}

	@GetMapping("/login")
	public String login()

	{
		return "login";
	}
	

}
