package com.project.model;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Component;

@Component
public class User {

	private String name;
	private String username;
	private String mobile;
	private String blood;
	private String state;
	private String city;
	private String gender;
	private String password;
	
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date dateOfBirth;
	
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date dateOfLastDonation;
	

	public User() {
		super();

	}

	




	public User(String name, String username, String mobile, String blood, String state, String city, String gender,
			String password, Date dateOfBirth, Date dateOfLastDonation) {
		super();
		this.name = name;
		this.username = username;
		this.mobile = mobile;
		this.blood = blood;
		this.state = state;
		this.city = city;
		this.gender = gender;
		this.password = password;
		this.dateOfBirth = dateOfBirth;
		this.dateOfLastDonation = dateOfLastDonation;
	}






	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	
	public String getUsername() {
		return username;
	}






	public void setUsername(String username) {
		this.username = username;
	}






	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getBlood() {
		return blood;
	}

	public void setBlood(String blood) {
		this.blood = blood;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	
	

	public Date getDateOfLastDonation() {
		return dateOfLastDonation;
	}



	public void setDateOfLastDonation(Date dateOfLastDonation) {
		this.dateOfLastDonation = dateOfLastDonation;
	}



	@Override
	public String toString() {
		return "User [name=" + name + ", username=" + username + ", mobile=" + mobile + ", blood=" + blood + ", state=" + state
				+ ", city=" + city + ", gender=" + gender + ", password=" + password + ", dateOfBirth=" + dateOfBirth
				+ ", dateOfLastDonation=" + dateOfLastDonation + "]";
	}



	
	
	

	

	
	

}
