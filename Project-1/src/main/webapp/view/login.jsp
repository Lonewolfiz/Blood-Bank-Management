
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<title>Login Page</title>
<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css"
	rel="stylesheet"
	integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6"
	crossorigin="anonymous">
<script
	src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js"
	integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf"
	crossorigin="anonymous"></script>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta charset="utf-8">

<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet"
	href="https://fonts.googleapis.com/css?family=Lato">
<link rel="stylesheet"
	href="https://fonts.googleapis.com/css?family=Montserrat">
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css" />
<link rel="stylesheet" type="text/css" href="/css/bgstyle.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

</head>
<body>

	<div class="w3-bar w3-black w3-card w3-left-align w3-large">
		<a
				class="w3-bar-item w3-button w3-hide-medium w3-hide-large w3-right w3-padding-large w3-hover-white w3-large w3-red"
				href="javascript:void(0);" onclick="myFunction()"
				title="Toggle Navigation Menu"><i class="fa fa-bars"></i></a> <a
				
				class="w3-bar-item w3-button w3-padding-large w3-hover-white"><i class="fa fa-home"></i> Home</a> <a
				
				class="w3-bar-item w3-button w3-hide-small w3-padding-large w3-right w3-white"><i class="fa fa-sign-in" aria-hidden="true"></i> Sign-In</a>
			<a href="/register"
				class="w3-bar-item w3-button w3-hide-small w3-padding-large w3-right w3-hover-white"><i class="fa fa-user-plus" aria-hidden="true"></i> Sign-Up</a>
				<a href="/infohome"
			class="w3-bar-item w3-button w3-hide-small w3-padding-large w3-hover-white w3-right"><i
			class="fa fa-info-circle" aria-hidden="true"></i> Info</a>

	</div>



	<div class="bg-image-login"></div>

	<div class="bg-text-login">


		<form class="row g-3" action="/login" method="POST">
			<div class="col-md-3">
				<label for="userName" class="form-label">Email</label>
			</div>
			<div class="col-md-7">
				<input type="email" class="form-control" name="username"
					placeholder="abc@example.com" required>
			</div>
			<div class="col-md-3">
				<label for="password" class="form-label">Password</label>
			</div>
			<div class="col-md-7">
				<input type="password" class="form-control" name="password" required>
			</div>


			<div class="col-12">
				<button type="submit" class="btn btn-outline-primary"
					onclick="/home" >Sign In</button>
			</div>
			 
			<a href="/sendemail">Reset password</a> ${message}

		</form>

	</div>

</body>
</html>