<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head> 
<meta charset="ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet"
	href="https://fonts.googleapis.com/css?family=Lato">
<link rel="stylesheet"
	href="https://fonts.googleapis.com/css?family=Montserrat">
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css" />
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>
<link rel="stylesheet" type="text/css" href="/css/bgstyle.css">
</head>
<body>
	<div class="w3-bar w3-black w3-card w3-left-align w3-large">
		<a
				class="w3-bar-item w3-button w3-hide-medium w3-hide-large w3-right w3-padding-large w3-hover-white w3-large w3-red"
				href="javascript:void(0);" onclick="myFunction()"
				title="Toggle Navigation Menu"><i class="fa fa-bars"></i></a> <a
				
				class="w3-bar-item w3-button w3-padding-large w3-hover-white"><i class="fa fa-home"></i> Home</a> <a
				
				class="w3-bar-item w3-button w3-hide-small w3-padding-large w3-right w3-white"><i class="fa fa-sign-in" aria-hidden="true"></i> Sign-In</a>
			<a href="/register"
				class="w3-bar-item w3-button w3-hide-small w3-padding-large w3-right w3-hover-white"><i class="fa fa-user-plus" aria-hidden="true"></i> Sign-Up</a>
				<a href="/infohome"
			class="w3-bar-item w3-button w3-hide-small w3-padding-large w3-hover-white w3-right"><i
			class="fa fa-info-circle" aria-hidden="true"></i> Info</a>


	</div>



	<div class="bg-image-login"></div>

	<div class="bg-text-login">
		<form class="row g-3" method="post" action="sendemail">
			<div class="col-md-6">
				<label for="mail" class="form-label">Enter Your Email</label>
				
			</div>
			<div class="col-md-6">
			<input type="email" class="form-control" name="email" required>
			</div>
			<div class="col-12">
				<input type="submit" class="btn btn-outline-primary" value="SUBMIT">
			</div>
			
			
		</form>
		</div>
</body>
</html>


