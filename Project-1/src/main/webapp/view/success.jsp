<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet"
	href="https://fonts.googleapis.com/css?family=Lato">
<link rel="stylesheet"
	href="https://fonts.googleapis.com/css?family=Montserrat">
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css" />
 

<script type="text/javascript" src="/js/validation.js"></script>
<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css"
	rel="stylesheet"
	integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6"
	crossorigin="anonymous">
<script
	src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js"
	integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf"
	crossorigin="anonymous"></script>
<meta name="viewport" content="width=device-width, initial-scale=1">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

<link rel="stylesheet" type="text/css" href="/css/bgstyle.css">


<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script>
	function checkPasswordMatch() {
		var password = $("#txtNewPassword").val();
		var confirmPassword = $("#txtConfirmPassword").val();
		if (password != confirmPassword)
			$("#CheckPasswordMatch").html("Passwords does not match!");
		else
			$("#CheckPasswordMatch").html("Passwords matches.");
	}
	$(document).ready(function() {
		$("#txtConfirmPassword").keyup(checkPasswordMatch);
	});
</script>

</head>
<body>
	<div class="w3-bar w3-black w3-card w3-left-align w3-large">
		<a
				class="w3-bar-item w3-button w3-hide-medium w3-hide-large w3-right w3-padding-large w3-hover-white w3-large w3-red"
				href="javascript:void(0);" onclick="myFunction()"
				title="Toggle Navigation Menu"><i class="fa fa-bars"></i></a> <a
				
				class="w3-bar-item w3-button w3-padding-large w3-hover-white"><i class="fa fa-home"></i> Home</a> <a
				
				class="w3-bar-item w3-button w3-hide-small w3-padding-large w3-right w3-white"><i class="fa fa-sign-in" aria-hidden="true"></i> Sign-In</a>
			<a href="/register"
				class="w3-bar-item w3-button w3-hide-small w3-padding-large w3-right w3-hover-white"><i class="fa fa-user-plus" aria-hidden="true"></i> Sign-Up</a>
				<a href="/infohome"
			class="w3-bar-item w3-button w3-hide-small w3-padding-large w3-hover-white w3-right"><i
			class="fa fa-info-circle" aria-hidden="true"></i> Info</a>

	</div>



		<div class="bg-image-login"></div>

		<div class="bg-text-login">
			<form class="row g-3" action="changePassword" modelAttribute="changePassword" method="post">
				<div name="frmCheckPassword" id="frmCheckPassword" class="col-md-6">
					<label for="password" class="form-label">Password</label> <span>*</span>
					<a href="#" data-toggle="popover"
						title="At least 8 charactersÃ¢??the more characters, the better.
A mixture of both uppercase and lowercase letters.
A mixture of letters and numbers.
Inclusion of at least one special character, e.g., ! @ # ? ] Note: do not use < or >"
						data-content="Some content inside the popover">&#9888;</a> <input
						type="password" name="password" id="txtNewPassword"
						path="password" class="demoInputBox form-control"
						onKeyUp="checkPasswordStrength();" />
					<div id="password-strength-status"></div>
				</div>
				<div class="col-md-6">
					<label for="cpassword" class="form-label">Confirm Password</label>
					<span>*</span> <input type="password" class="form-control"
						name="confirmPassword" id="txtConfirmPassword">
					<div style="color: green;" id="CheckPasswordMatch"></div>
				</div>
                 <div class="col-md-12">
                 <input type="submit" class="btn btn-outline-primary" value="Change Password" >
                 </div>
			</form>

		</div>
		
</body>
</html>