<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet"
	href="https://fonts.googleapis.com/css?family=Lato">
<link rel="stylesheet"
	href="https://fonts.googleapis.com/css?family=Montserrat">
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- Add icon library -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css"
	rel="stylesheet"
	integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6"
	crossorigin="anonymous">
<script
	src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js"
	integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf"
	crossorigin="anonymous"></script>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
<link rel="stylesheet" type="text/css" href="/css/info.css">
</head>
<body id="section1">


	<div class="w3-bar w3-black w3-card w3-left-align w3-large">
		<a
			class="w3-bar-item w3-button w3-hide-medium w3-hide-large w3-right w3-padding-large w3-hover-white w3-large w3-red"
			href="javascript:void(0);" onclick="myFunction()"
			title="Toggle Navigation Menu"><i class="fa fa-bars"></i></a> <a
			href="/Homepage"
			class="w3-bar-item w3-button w3-padding-large w3-hover-white"><i
			class="fa fa-home"></i> Home</a> <a href="/login"
			class="w3-bar-item w3-button w3-hide-small w3-padding-large w3-right w3-hover-white"><i
			class="fa fa-sign-in" aria-hidden="true"></i> Sign-In</a> <a
			href="/register"
			class="w3-bar-item w3-button w3-hide-small w3-padding-large w3-right w3-hover-white"><i
			class="fa fa-user-plus" aria-hidden="true"></i> Sign-Up</a> <a
			class="w3-bar-item w3-button w3-hide-small w3-padding-large w3-white w3-right"><i
			class="fa fa-info-circle" aria-hidden="true"></i> Info</a>
	</div>
	<header>
		<center>
			<i><h3 style="color: white;">Click the picture and explore</h3></i>
		</center>
	</header>
	<center>
		<div class="row">
			<div class="column">
				<a href="#section2"> <img src="images/a+ve.png" width="200"
					height="200">
				</a>
			</div>
			<div class="column">
				<a href="#section3"> <img src="images/a-ve.png" width="200"
					height="200">
				</a>

			</div>
			<div class="column">
				<a href="#section4"> <img src="images/ab+ve.png" width="200"
					height="200"></a>

			</div>


			<div class="column">
				<a href="#section5"> <img src="images/ab-ve.png" width="200"
					height="200">
				</a>
			</div>


			<div class="column">
				<a href="#section6"> <img src="images/b+ve.png" width="200"
					height="200">
				</a>
			</div>
			<div class="column">
				<a href="#section7"> <img src="images/b-ve.png" width="200"
					height="200"></a>
			</div>

			<div class="column">
				<a href="#section8"> <img src="images/o+ve.png" width="200"
					height="200"></a>
			</div>
			<div class="column">
				<a href="#section9"> <img src="images/O-ve.png" width="200"
					height="200">
				</a>
			</div>

		</div>
		<br> <br>
	</center>
	<center>
		<h2 id="section2" class="line"
			style="color: white; text-decoration-color: white;">A Positive</h2>
		<br>
		<h5>
			<i>If your blood is A positive (A+), it means that your blood
				contains type-A antigens with the presence of a protein called the
				rhesus (Rh) factor. Antigens are markers on the surface of a blood
				cell. According to the American Red Cross, this is one of the most
				common blood types.One in three people in the United States has A
				positive blood type, making it the second most common in the
				country. As such, it can be a good type to have if a person in the
				U.S. needs a blood transfusion or wishes to donate blood. </i>
		</h5>

	</center>
	<br>
	<center>
		<h5 style="color: white;" class="line">Donate And Recive</h5>
	</center>

	<img
		src="https://nhsbtdbe.blob.core.windows.net/umbraco-assets-corp/15064/29720-000np-know-your-type-web-buttons-400px-x-170px-blood-type-aplus.png"
		style="float: left;" width="300" height="130">
	<img
		src="https://nhsbtdbe.blob.core.windows.net/umbraco-assets-corp/15064/29720-000np-know-your-type-web-buttons-400px-x-170px-blood-type-aplus.png"
		style="float: right;" width="300" height="130">
	<center>
		<table>
			<tr>
				<th>Donate_to</th>
				<th>Recive_from</th>
			</tr>
			<tr>
				<td>A+</td>
				<td>A+</td>
			</tr>
			<tr>
				<td>AB+</td>
				<td>A-</td>
			</tr>
			<tr>
				<td>-</td>
				<td>O+</td>
			</tr>
			<tr>
				<td>-</td>
				<td>O-</td>
			</tr>

		</table>
	</center>
	<br>
	<center>
		<h5 style="color: white;" class="line">Facts</h5>
	</center>
	<br>
	<h5>
		<i>34% of people are A+, making it the second most common blood
			type. A+ platelets are always high in demand for patients undergoing
			chemotherapy.</i>
	</h5>
	<br>
	<h5>
		<i>28.1% of Indians have A positive blood group.</i>
	</h5>
	<br>
	<h5>
		<i>The countrie with the highest prevalence of type A positive
			blood is Armenia with 46.3%</i>
	</h5>
	<br>
	<h5>
		<i>People with type A blood are more likely to develop stomach
			cancer.</i>
	</h5>
	<br>
	<center>
		<h5 style="color: white;" class="line">Diet has to followed</h5>
	</center>
	<img
		src="https://blogs.biomedcentral.com/on-medicine/wp-content/uploads/sites/6/2019/09/iStock-1131794876.t5d482e40.m800.xtDADj9SvTVFjzuNeGuNUUGY4tm5d6UGU5tkKM0s3iPk-620x342.jpg"
		style="float: right;" width="500" height="300">
	<br>
	<h5>
		<i>

			<ul>
				<li>soy protein such as tofu</li>
				<li>certain grains, such as spelt, hulled barley, and sprouted
					bread</li>
				<li>walnuts, pumpkin seeds, and peanuts</li>
				<li>olive oil</li>
				<li>certain fruits, such as blueberries and elderberries</li>
				<li>certain kinds of beans and legumes</li>
				<li>certain vegetables, especially dark, leafy greens, such as
					kale, Swiss chard, and spinach garlic and onions</li>
				<li>cold-water fish, such as sardines and salmon</li>
				<li>limited amounts of chicken and turkey</li>
				<li>green tea</li>
				<li>ginger</li>
			</ul> <br> <br>

		</i>
	</h5>

	<center>
		<h5 style="color: white;" class="line">Personality</h5>
	</center>
	<br>
	<h5>
		<i>People in this category are very stubborn and are easily
			stressed. They have high levels of the stress hormone cortisol, and
			this makes them intense. They do not like fights, and they prefer to
			be in harmony with everyone, and they like to work in collaboration
			with others in the community. They tend to keep to themselves more so
			when they do not want to share their ideas or opinions. Some of the
			most common traits of blood type A are as follows. They are kind,
			shy, stubborn, attentive, composed, polite, tense, withdrawn,
			reliable, perfectionist, sensitive, responsible, tactful, timid,
			mild-mannered, anxious, earnest, reserved, and polite. The best
			traits of people with type A are gentle, loyal, organized,
			consistent, loyal, and perfectionists. Their bad traits are,
			obsessive, pessimistic, overly sensitive, fastidious, stubborn, and
			easily stressed.</i>
	</h5>
	<br>
	<center>
		<h5 style="color: white;" class="line">Persons having A+ve blood
			group</h5>
	</center>
	<br>
	<h5>
		1.Adolf Hitler <br> 2.Sarchin TendulkarS <br>3.Lionel Messi
	</h5>
	<br>
	<div class="row">
		<div class="column">
			<img src="https://static.dw.com/image/17886435_303.jpg" alt="Snow"
				style="width: 100%">
		</div>
		<div class="column">
			<img
				src="https://www.t20mumbai.com/static-assets/waf-images/cf/d1/88/16-9/803MPxT88Y.jpg"
				alt="Forest" style="width: 100%">
		</div>
		<div class="column">
			<img src="https://images.indianexpress.com/2020/09/lionel-messi.jpg"
				alt="Forest" style="width: 100%">
		</div>
	</div>
	<br>
	<br>
	<center>
		<a href="#section1">

			<button class="button button2">BacktoInfo</button>
		</a>
	</center>

	<br>
	<br>
	<br>
	<center>
		<h2 id="section3" style="color: white; text-decoration-color: white;"
			class="line">A Negetive</h2>
		<br>
		<h5>
			<i>A negative red blood cells can be used to treat around 40% of
				the population. However, A negative platelets are particularly
				important because they can be given to people from all blood groups.
				That's why A negative platelets are called the 'universal platelet
				type'.A- blood is typically transfused quickly because of the
				community's need, so it's constantly in demand. </i>
		</h5>
	</center>

	<br>
	<center>
		<h5 style="color: white;" class="line">Donate And Recive</h5>
	</center>

	<img
		src="https://www.blood.co.uk/remote.axd/nhsbtdbe.blob.core.windows.net/umbraco-assets-corp/15063/29720-000np-know-your-type-web-buttons-400px-x-170px-blood-type-a.png?anchor=center&mode=crop&width=600&height=255&rnd=131925388260000000&quality=70"
		style="float: left;" width="300" height="130">
	<img
		src="https://www.blood.co.uk/remote.axd/nhsbtdbe.blob.core.windows.net/umbraco-assets-corp/15063/29720-000np-know-your-type-web-buttons-400px-x-170px-blood-type-a.png?anchor=center&mode=crop&width=600&height=255&rnd=131925388260000000&quality=70"
		style="float: right;" width="300" height="130">

	<center>
		<table>
			<tr>
				<th>Donate_to</th>
				<th>Recive_from</th>
			</tr>
			<tr>
				<td>A+</td>
				<td>A-</td>
			</tr>
			<tr>
				<td>A-</td>
				<td>O-</td>
			</tr>
			<tr>
				<td>AB+</td>
				<td>-</td>
			</tr>
			<tr>
				<td>AB-</td>
				<td>-</td>
			</tr>

		</table>
	</center>
	<br>
	<center>
		<h5 style="color: white;" class="line">Facts</h5>
	</center>
	<br>
	<h5>
		<i>Only 1 in 16 people have A- blood.</i>
	</h5>
	<br>
	<h5>
		<i>1.36% of Indians have A positive blood group.</i>
	</h5>
	<br>
	<h5>
		<i>The countrie with the highest prevalence of type A positive
			blood is Brazil with 8%</i>
	</h5>
	<br>
	<h5>
		<i>People with type A blood are more likely to develop stomach
			cancer.</i>
	</h5>
	<br>
	<center>
		<h5 style="color: white;" class="line">Diet has to followed</h5>
	</center>
	<img
		src="https://blogs.biomedcentral.com/on-medicine/wp-content/uploads/sites/6/2019/09/iStock-1131794876.t5d482e40.m800.xtDADj9SvTVFjzuNeGuNUUGY4tm5d6UGU5tkKM0s3iPk-620x342.jpg"
		style="float: right;" width="500" height="300">
	<br>
	<h5>
		<i>

			<ul>
				<li>beef</li>
				<li>mutton</li>
				<li>cheeses, such as farmer, feta, mozzarella, and
					goatâ€™s cheese</li>
				<li>black eyed peas</li>
				<li>adzuki beans</li>
				<li>Essene bread and Ezekiel bread</li>
				<li>tomatoes artichoke chicory dandelion garlic horseradish</li>
				<li>cold-water fish, such as sardines and salmon</li>
				<li>limited amounts of chicken and turkey</li>
				<li>green tea</li>
				<li>ginger</li>
			</ul> <br> <br>
		</i>
	</h5>

	<center>
		<h5 style="color: white;" class="line">Personality</h5>
	</center>
	<br>
	<h5>
		<i>People with blood type A are clever, passionate, sensitive, and
			cooperative. They are loyal, patient, and they love peace. Sometimes,
			they may be overly sensitive about different things. For instance,
			they care a lot about etiquette as well as social standards. They do
			not like to break the set rules on etiquette or the laid down
			societal standards or rules. On occasions where they choose to break
			the rules, itâ€™s considered a self-fulfilling prophecy. A
			personalities are careful decision makers, and they take their time
			before they can settle on any decision. Besides, they are not good at
			multi-tasking, as they prefer to handle one task at a time. Blood
			type A are very organized, and they do not like haphazard actions.
			They prefer to keep everything neat and in the right place. They plan
			everything, and every task that they engage in is carried out with a
			lot of consistency and seriousness. Many people with OCD fall into
			this category and end up seeking help from a professional counselor
			who can help with their compulsive issues</i>
	</h5>
	<br>
	<center>
		<h5 style="color: white;" class="line">Persons having A-ve blood
			group</h5>
	</center>
	<br>
	<h5>
		1.Dhanush <br> 2.Virat Kohli
	</h5>
	<br>
	<div class="row">
		<div class="column">
			<img
				src="https://cdn.dnaindia.com/sites/default/files/styles/full/public/2018/07/31/711587-405138-308867-dhanush.jpg"
				alt="Snow" style="width: 100%">
		</div>
		<div class="column">
			<img
				src="https://cdn.sportspromedia.com/images/made/images/uploads/news/kohlivivo_630_354_80_s_c1.jpg"
				alt="Forest" style="width: 100%">
		</div>
	</div>
	<br>
	<br>

	<a href="#section1">
		<button class="button button2">Back to Info</button>
	</a>

	<br>
	<br>
	<br>
	<center>
		<h2 id="section4" style="color: white; text-decoration-color: white;"
			class="line">AB Positive</h2>
		<br>
		<h5>
			<i>AB+ blood has both A and B antigens on the red blood cells,
				neither of the antigens are present in the plasma. This makes AB+
				the universal plasma donor, meaning that AB+ plasma can be
				transfused into patients who have any other ABO blood type. The AB
				blood group is believed to be the newest blood type.AB positive
				blood type is known as the â€œuniversal recipientâ€ because
				AB positive patients can receive red blood cells from all blood
				types.</i>
		</h5>
	</center>

	<br>
	<center>
		<h5 style="color: white;" class="line">Donate And Recive</h5>
	</center>

	<img
		src="https://www.blood.co.uk/remote.axd/nhsbtdbe.blob.core.windows.net/umbraco-assets-corp/15066/29720-000np-know-your-type-web-buttons-400px-x-170px-blood-type-abplus.png?anchor=center&mode=crop&width=600&height=255&rnd=131925388280000000&quality=70"
		style="float: left;" width="300" height="130">
	<img
		src="https://www.blood.co.uk/remote.axd/nhsbtdbe.blob.core.windows.net/umbraco-assets-corp/15066/29720-000np-know-your-type-web-buttons-400px-x-170px-blood-type-abplus.png?anchor=center&mode=crop&width=600&height=255&rnd=131925388280000000&quality=70"
		style="float: right;" width="300" height="130">

	<center>
		<table>
			<tr>
				<th>Donate_to</th>
				<th>Recive_from</th>
			</tr>
			<tr>
				<td>AB+</td>
				<td>ALL</td>
			</tr>
			<tr>
				<td>-</td>
				<td>-</td>
			</tr>
			<tr>
				<td>-</td>
				<td>-</td>
			</tr>
			<tr>
				<td>-</td>
				<td>-</td>
			</tr>

		</table>
	</center>
	<br>
	<center>
		<h5 style="color: white;" class="line">Facts</h5>
	</center>
	<br>
	<h5>
		<i>Only 4% of population have AB+ blood.</i>
	</h5>
	<br>
	<h5>
		<i>11.32% of North koreans have A positive blood group.</i>
	</h5>
	<br>
	<h5>
		<i>It is an â€œuniversal recipientâ€ because AB positive
			patients can receive red blood cells from all blood types.</i>
	</h5>
	<br>
	<h5>
		<i>People with type AB blood are much more likely to develop
			cognitive issues.</i>
	</h5>
	<br>
	<center>
		<h5 style="color: white;" class="line">Diet has to followed</h5>
	</center>
	<img
		src="https://blogs.biomedcentral.com/on-medicine/wp-content/uploads/sites/6/2019/09/iStock-1131794876.t5d482e40.m800.xtDADj9SvTVFjzuNeGuNUUGY4tm5d6UGU5tkKM0s3iPk-620x342.jpg"
		style="float: right;" width="500" height="300">
	<br>
	<h5>
		<i>

			<ul>
				<li>Tofu</li>
				<li>seafood</li>
				<li>rice + daal, roti + daal, dalia, khichdi, and brown rice</li>
				<li>yoghurt</li>
				<li>adzuki beans</li>
				<li>goat milk, egg</li>
				<li>tomatoes artichoke chicory dandelion garlic horseradish</li>
				<li>cold-water fish, such as sardines and salmon</li>
				<li>walnuts, millets, oats, rye</li>
				<li>Coffee</li>
				<li>broccoli, cauliflower, beets, cucumber</li>
			</ul> <br> <br>
		</i>
	</h5>

	<center>
		<h5 style="color: white;" class="line">Personality</h5>
	</center>
	<br>
	<h5>
		<i>Blood type AB is a mix of A and B personality types. People
			consider them complicated and double-sided. For instance, they can be
			outgoing just like Bs and shy like As. At times, people view them as
			having double personalities, and they keep their true personalities
			from strangers. It is hard for a stranger to instantly decide which
			personality people with type AB have until they get to know them.
			They are the rarest in the world. ABs are popular and charming, and
			they make friends easily. When in the company of an AB personality,
			there can never be a dull moment. They are fun and exciting friends
			to have around. Small stuff does not bother AB personalities, but
			they are poor in handling stress.</i>
	</h5>
	<br>
	<center>
		<h5 style="color: white;" class="line">Persons having AB+ve blood
			group</h5>
	</center>
	<br>
	<h5>
		1.Thomas edision <br> 2.Jacki chan<br>3.Barack Obama.
	</h5>
	<br>
	<div class="row">
		<div class="column">
			<img
				src="https://www.thoughtco.com/thmb/7lViK0NG8BKCP79VOGG5L0xYEA4=/1500x844/smart/filters:no_upscale()/ThomasEdison-58b82fee3df78c060e6505b7.jpg"
				alt="Snow" style="width: 100%">
		</div>
		<div class="column">
			<img
				src="https://thumbor.forbes.com/thumbor/fit-in/1200x0/filters%3Aformat%28jpg%29/https%3A%2F%2Fblogs-images.forbes.com%2Fzackomalleygreenburg%2Ffiles%2F2015%2F06%2F0626_celeb100-jackie-chan_1200x675.jpg"
				alt="Forest" style="width: 100%">

		</div>
		<div class="column">
			<img
				src="https://cdn.theatlantic.com/thumbor/uWsayd1hU1kkHwIBVafd7JxliZQ=/0x0:3000x1688/1600x900/media/img/mt/2020/11/GettyImages_83513179/original.jpg"
				alt="Forest" style="width: 100%">

		</div>
	</div>
	<br>
	<br>
	<center>
		<a href="#section1">

			<button class="button button2">BacktoInfo</button>
		</a>
	</center>
	<br>
	<br>
	<br>
	<center>
		<h2 id="section5" style="color: white; text-decoration-color: white;"
			class="line">AB Negetive</h2>
		<br>
		<h5>
			<i>AB negative is the rarest blood type in the ABO blood group,
				accounting for just 1% of our blood donors. In total only 3% of
				donors belong to the AB blood group.Every two seconds, someone in
				the U.S needs a transfusion of red blood cells. ... Less than 1% of
				the U.S. population have AB negative blood, making it the least
				common blood type among Americans. Patients with AB negative blood
				type can receive red blood cells from all negative blood types.</i>
		</h5>
	</center>

	<br>
	<center>
		<h5 style="color: white;" class="line">Donate And Recive</h5>
	</center>

	<img
		src="https://nhsbtdbe.blob.core.windows.net/umbraco-assets-corp/15065/29720-000np-know-your-type-web-buttons-400px-x-170px-blood-type-ab.png"
		style="float: left;" width="300" height="130">
	<img
		src="https://nhsbtdbe.blob.core.windows.net/umbraco-assets-corp/15065/29720-000np-know-your-type-web-buttons-400px-x-170px-blood-type-ab.png"
		style="float: right;" width="300" height="130">

	<center>
		<table>
			<tr>
				<th>Donate_to</th>
				<th>Recive_from</th>
			</tr>
			<tr>
				<td>AB+</td>
				<td>AB-</td>
			</tr>
			<tr>
				<td>AB-</td>
				<td>A-</td>
			</tr>
			<tr>
				<td>-</td>
				<td>B-</td>
			</tr>
			<tr>
				<td>-</td>
				<td>O-</td>
			</tr>

		</table>
	</center>
	<br>
	<center>
		<h5 style="color: white;" class="line">Facts</h5>
	</center>
	<br>
	<h5>
		<i>Only 0.6% of population have AB- blood.</i>
	</h5>
	<br>
	<h5>
		<i>Only 0.46%of Indians have AB- blood.</i>
	</h5>
	<br>
	<h5>
		<i>AB negative donations are extremely versatile, but because it
			is the rarest blood </i>
	</h5>
	<br>
	<h5>
		<i>People with type AB blood are much more likely to develop
			cognitive issues.</i>
	</h5>
	<br>
	<center>
		<h5 style="color: white;" class="line">Diet has to followed</h5>
	</center>
	<img
		src="https://blogs.biomedcentral.com/on-medicine/wp-content/uploads/sites/6/2019/09/iStock-1131794876.t5d482e40.m800.xtDADj9SvTVFjzuNeGuNUUGY4tm5d6UGU5tkKM0s3iPk-620x342.jpg"
		style="float: right;" width="500" height="300">
	<br>
	<h5>
		<i>

			<ul>
				<li>Tofu</li>
				<li>seafood</li>
				<li>rice + daal, roti + daal, dalia, khichdi, and brown rice</li>
				<li>yoghurt</li>
				<li>adzuki beans</li>
				<li>goat milk, egg</li>
				<li>tomatoes artichoke chicory dandelion garlic horseradish</li>
				<li>cold-water fish, such as sardines and salmon</li>
				<li>walnuts, millets, oats, rye</li>
				<li>Coffee</li>
				<li>broccoli, cauliflower, beets, cucumber</li>
			</ul> <br> <br>
		</i>
	</h5>

	<center>
		<h5 style="color: white;" class="line">Personality</h5>
	</center>
	<br>
	<h5>
		<i>ABs are empathetic, and they are always careful when dealing
			with others. They make sure that they consider other people's point
			of view. These people have exceptional logical and analytical skills,
			and they are seen as humanists. <br> Some of their good traits
			are; caring, charming, controlled, dependable, cool, composed,
			sociable, dream chaser, trustworthy, rational, creative, adaptable,
			and diplomatic. Their negative traits are such as complicated,
			self-centered, irresponsible, vulnerable, indecisive, forgetful,
			unforgiving, critical, and two-faced.
		</i>
	</h5>
	<br>
	<center>
		<h5 style="color: white;" class="line">Persons having AB-ve blood
			group</h5>
	</center>
	<br>
	<h5>
		1.Donald Trump <br> 2.Charlie Chaplin<br>
	</h5>
	<br>
	<div class="row">
		<div class="column">
			<img
				src="https://ichef.bbci.co.uk/news/976/cpsprodpb/17EF9/production/_117614089_05c5a5bd-e3eb-4bdf-8235-71ed7f61c4fc.jpg"
				alt="Snow" style="width: 100%">
		</div>
		<div class="column">
			<img
				src="https://variety.com/wp-content/uploads/2020/10/Charlie-Chaplin.jpg?w=1000"
				alt="Forest" style="width: 100%">

		</div>
	</div>
	<br>
	<br>
	<center>
		<a href="#section1">

			<button class="button button2">BacktoInfo</button>
		</a>
	</center>
	<br>
	<br>
	<br>
	<center>
		<h2 id="section6" style="color: white; text-decoration-color: white;"
			class="line">B Positive</h2>
		<br>
		<h5>
			<i>B positive is an important blood type for treating people with
				sickle cell disease and thalassemia who need regular transfusions.
				These conditions affect South Asian and Black communities where B
				positive blood is more common. There is currently a very high demand
				for B positive donations with the subtype Ro.B+ is a rare blood type
				that holds tremendous power. Only 8% of the population has B+ blood.
				B+ blood donors have two ways of targeting the power of their
				donation. The most preferred donation method is to donate platelets.</i>
		</h5>
	</center>

	<br>
	<center>
		<h5 style="color: white;" class="line">Donate And Recive</h5>
	</center>

	<img
		src="https://nhsbtdbe.blob.core.windows.net/umbraco-assets-corp/15068/29720-000np-know-your-type-web-buttons-400px-x-170px-blood-type-bplus.png"
		style="float: left;" width="300" height="130">
	<img
		src="https://nhsbtdbe.blob.core.windows.net/umbraco-assets-corp/15068/29720-000np-know-your-type-web-buttons-400px-x-170px-blood-type-bplus.png"
		style="float: right;" width="300" height="130">

	<center>
		<table>
			<tr>
				<th>Donate_to</th>
				<th>Recive_from</th>
			</tr>
			<tr>
				<td>AB+</td>
				<td>B+</td>
			</tr>
			<tr>
				<td>B+</td>
				<td>B-</td>
			</tr>
			<tr>
				<td>-</td>
				<td>AB+</td>
			</tr>
			<tr>
				<td>-</td>
				<td>AB-</td>
			</tr>

		</table>
	</center>
	<br>
	<center>
		<h5 style="color: white;" class="line">Facts</h5>
	</center>
	<br>
	<h5>
		<i>About 9% of the World population have B positive blood.</i>
	</h5>
	<br>
	<h5>
		<i>32.09% of Indians have B+ blood.</i>
	</h5>
	<br>
	<h5>
		<i> Thailand has the highest number of B+ blood type with 36.08%.</i>
	</h5>
	<br>
	<h5>
		<i>People with type B blood are more susceptible to gastroenteric
			cancers.</i>
	</h5>
	<br>
	<center>
		<h5 style="color: white;" class="line">Diet has to followed</h5>
	</center>
	<img
		src="https://blogs.biomedcentral.com/on-medicine/wp-content/uploads/sites/6/2019/09/iStock-1131794876.t5d482e40.m800.xtDADj9SvTVFjzuNeGuNUUGY4tm5d6UGU5tkKM0s3iPk-620x342.jpg"
		style="float: right;" width="500" height="300">
	<br>
	<h5>
		<i>

			<ul>
				<li>Green vegetables</li>
				<li>eggs</li>
				<li>low-fat dairy</li>
				<li>oats</li>
				<li>milk products</li>
				<li>animal protein</li>
				<li>oat bran</li>
				<li>cold-water fish, such as sardines and salmon</li>
				<li>oat meal and quinoa.</li>
				<li>paneer</li>
				<li>broccoli, cauliflower, beets, cucumber</li>
			</ul> <br> <br>
		</i>
	</h5>

	<center>
		<h5 style="color: white;" class="line">Personality</h5>
	</center>
	<br>
	<h5>
		<i>People with this blood type are famous for their creativity.
			They make their decisions very quickly, and they are not good at
			taking orders. When they focus on something, they put their all into
			it, and they are unlikely to let go even if the goal is unachievable.
			They have a very strong drive or desire to be the best at anything
			that they have set their minds to do. Nevertheless, they are poor at
			multi-tasking, and they are likely to neglect other important tasks
			and put all their focus on whatever they have set their mind on at
			the moment. <br> Type B people can be thoughtful and empathetic.
			They are good at understanding other people's point of view and do
			not like challenging or confronting others. People with blood type B
			make good and reliable friends.
		</i>
	</h5>
	<br>
	<center>
		<h5 style="color: white;" class="line">Persons having B+ve blood
			group</h5>
	</center>
	<br>
	<h5>
		1.Joseph Vijay <br> 2.Leonardo DiCaprio<br>
	</h5>
	<br>
	<div class="row">
		<div class="column">
			<img
				src="https://i.pinimg.com/736x/2c/f3/fd/2cf3fdbf3387c1975a092ae981ba5b8b.jpg"
				alt="Snow" style="width: 100%">
		</div>
		<div class="column">
			<img
				src="https://www.indiewire.com/wp-content/uploads/2019/07/Screen-Shot-2019-07-24-at-12.18.54-PM.png?w=780"
				alt="Forest" style="width: 100%">

		</div>
	</div>
	<br>
	<br>
	<center>
		<a href="#section1">

			<button class="button button2">BacktoInfo</button>
		</a>
	</center>
	<br>
	<br>
	<br>
	<center>
		<h2 id="section7" style="color: white; text-decoration-color: white;"
			class="line">B Negetive</h2>
		<br>
		<h5>
			<i>Less than 2% of the population have B negative blood. B
				negative red blood cells can be given to both B and AB patients. B
				negative patients can only receive blood from other B negative
				donors or from type O negative donors (who are the universal
				donors). Since both of those types are fairly rare, the Red Cross
				works hard to ensure that sufficient supplies are always available.
			</i>
		</h5>
	</center>

	<br>
	<center>
		<h5 style="color: white;" class="line">Donate And Recive</h5>
	</center>

	<img
		src="https://nhsbtdbe.blob.core.windows.net/umbraco-assets-corp/15067/29720-000np-know-your-type-web-buttons-400px-x-170px-blood-type-b.png"
		style="float: left;" width="300" height="130">
	<img
		src="https://nhsbtdbe.blob.core.windows.net/umbraco-assets-corp/15067/29720-000np-know-your-type-web-buttons-400px-x-170px-blood-type-b.png"
		style="float: right;" width="300" height="130">

	<center>
		<table>
			<tr>
				<th>Donate_to</th>
				<th>Recive_from</th>
			</tr>
			<tr>
				<td>B+</td>
				<td>B-</td>
			</tr>
			<tr>
				<td>B-</td>
				<td>O-</td>
			</tr>
			<tr>
				<td>AB+</td>
				<td>-</td>
			</tr>
			<tr>
				<td>AB-</td>
				<td>-</td>
			</tr>

		</table>
	</center>
	<br>
	<center>
		<h5 style="color: white;" class="line">Facts</h5>
	</center>
	<br>
	<h5>
		<i>B- is only found in 1 in every 61 people, making it extremely
			rare.</i>
	</h5>
	<br>
	<h5>
		<i>Only 2.01% of Indians have B+ blood.</i>
	</h5>
	<br>
	<h5>
		<i>Every two seconds, someone needs blood, so B- is in high demand
			constantly.</i>
	</h5>
	<br>
	<h5>
		<i>People with type B blood are more susceptible to gastroenteric
			cancers.</i>
	</h5>
	<br>
	<center>
		<h5 style="color: white;" class="line">Diet has to followed</h5>
	</center>
	<img
		src="https://blogs.biomedcentral.com/on-medicine/wp-content/uploads/sites/6/2019/09/iStock-1131794876.t5d482e40.m800.xtDADj9SvTVFjzuNeGuNUUGY4tm5d6UGU5tkKM0s3iPk-620x342.jpg"
		style="float: right;" width="500" height="300">
	<br>
	<h5>
		<i>

			<ul>
				<li>Green vegetables</li>
				<li>eggs</li>
				<li>low-fat dairy</li>
				<li>oats</li>
				<li>milk products</li>
				<li>animal protein</li>
				<li>oat bran</li>
				<li>cold-water fish, such as sardines and salmon</li>
				<li>oat meal and quinoa.</li>
				<li>paneer</li>
				<li>broccoli, cauliflower, beets, cucumber</li>
			</ul> <br> <br>
		</i>
	</h5>

	<center>
		<h5 style="color: white;" class="line">Personality</h5>
	</center>
	<br>
	<h5>
		<i>Some of the most common positive traits of people with type B
			are such as curious, relaxed, strong, adventurous, creative,
			passionate, active, outgoing, and cheerful. On the other hand, the
			negative traits are, wild, erratic, unforgiving, selfish,
			uncooperative, irresponsible, and unpredictable. <br> They face
			a lot of discrimination because of their negative personalities such
			as selfishness and uncooperativeness at times. The society mainly
			focuses on the negative side of people with blood type B, even though
			they also have their good side. As a result, they tend to be loners,
			and they isolate themselves from others. People with type B may be
			harassed and called out for their personalities, so when they display
			a type B trait, itâ€™s likely to be labeled a self-fulfilling
			prophecy.
		</i>
	</h5>
	<br>
	<center>
		<h5 style="color: white;" class="line">Persons having B-ve blood
			group</h5>
	</center>
	<br>
	<h5>
		1.Johnny Depp <br> 2.Yuvraj singh<br>
	</h5>
	<br>
	<div class="row">
		<div class="column">
			<img
				src="https://img.theweek.in/content/dam/week/news/entertainment/images/2019/4/25/Johnny-Depp-dating.jpg"
				alt="Snow" style="width: 100%">
		</div>
		<div class="column">
			<img
				src="https://cricket.yahoo.net/static-assets/waf-images/48/36/33/16-9/796-597/a8d9a4ca9fc9b586297b11066c0973cc.jpeg"
				alt="Forest" style="width: 100%">

		</div>
	</div>
	<br>
	<br>
	<center>
		<a href="#section1">

			<button class="button button2">
				BacktoInfo
				<button>
		</a>
	</center>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<center>
		<h2 id="section8" style="color: white; text-decoration-color: white;"
			class="line">O Positive</h2>
		<br>
		<h5>
			<i>Type O positive blood is critical in trauma care. Those with O
				positive blood can only receive transfusions from O positive or O
				negative blood types. Type O positive blood is one of the first to
				run out during a shortage due to its high demand.Type O positive
				blood is given to patients more than any other blood type, which is
				why itâ€™s considered the most needed blood type. </i>
		</h5>
	</center>

	<br>
	<center>
		<h5 style="color: white;" class="line">Donate And Recive</h5>
	</center>

	<img
		src="https://nhsbtdbe.blob.core.windows.net/umbraco-assets-corp/15070/29720-000np-know-your-type-web-buttons-400px-x-170px-blood-type-oplus.png"
		style="float: left;" width="300" height="130">
	<img
		src="https://nhsbtdbe.blob.core.windows.net/umbraco-assets-corp/15070/29720-000np-know-your-type-web-buttons-400px-x-170px-blood-type-oplus.png"
		style="float: right;" width="300" height="130">

	<center>
		<table>
			<tr>
				<th>Donate_to</th>
				<th>Recive_from</th>
			</tr>
			<tr>
				<td>A+</td>
				<td>O+</td>
			</tr>
			<tr>
				<td>B+</td>
				<td>O-</td>
			</tr>
			<tr>
				<td>AB+</td>
				<td>-</td>
			</tr>
			<tr>
				<td>O-</td>
				<td>-</td>
			</tr>

		</table>
	</center>
	<br>
	<center>
		<h5 style="color: white;" class="line">Facts</h5>
	</center>
	<br>
	<h5>
		<i>38% of the population has O positive blood, making it the most
			common blood type.</i>
	</h5>
	<br>
	<h5>
		<i>32.53% of Indians have O+ blood.</i>
	</h5>
	<br>
	<h5>
		<i>People with type O blood are the least likely to suffer from
			cardiovascular issues.</i>
	</h5>
	<br>
	<h5>
		<i>People with type O blood are less susceptible to malaria.</i>
	</h5>
	<br>
	<center>
		<h5 style="color: white;" class="line">Diet has to followed</h5>
	</center>
	<img
		src="https://blogs.biomedcentral.com/on-medicine/wp-content/uploads/sites/6/2019/09/iStock-1131794876.t5d482e40.m800.xtDADj9SvTVFjzuNeGuNUUGY4tm5d6UGU5tkKM0s3iPk-620x342.jpg"
		style="float: right;" width="500" height="300">
	<br>
	<h5>
		<i>

			<ul>
				<li>Green vegetables</li>
				<li>meat (particularly lean meat and seafood for weight loss)</li>
				<li>eat lots of meat</li>
				<li>fish</li>
				<li>milk products</li>
				<li>seafood</li>
				<li>oat bran</li>
				<li>fruit but limit grains, beans, and legumes.</li>
				<li>oat meal and quinoa.</li>
				<li>olive oil</li>
				<li>broccoli, cauliflower, beets, cucumber</li>
			</ul> <br> <br>
		</i>
	</h5>

	<center>
		<h5 style="color: white;" class="line">Personality</h5>
	</center>
	<br>
	<h5>
		<i>People with O are outgoing, go-getters, and daring. They
			usually set high standards for themselves, and they do all they can
			to achieve them. Os have excellent leadership capabilities. Little
			things do not concern them, and this makes them appear as selfish to
			people in blood group A who are overly sensitive. People with blood
			group O are generous and kind-hearted. Most people love being O. O
			personalities adapt well to change. They are flexible and resilient
			and can do relatively well even in tough situations.</i>
	</h5>
	<br>
	<center>
		<h5 style="color: white;" class="line">Persons having O+ve blood
			group</h5>
	</center>
	<br>
	<h5>
		1.MS Dhoni <br> 2.Queen Elizabeth II<br>
	</h5>
	<br>
	<div class="row">
		<div class="column">
			<img
				src="https://cdn.dnaindia.com/sites/default/files/styles/full/public/2019/06/26/840862-22afp-afp1hq8kp.jpg"
				alt="Snow" style="width: 100%">
		</div>
		<div class="column">
			<img
				src="https://us.hola.com/images/025c-0f3a32acba52-7d321dd02c11-1000/horizontal-1150/queen-elizabeth-has-sent-a-message-to-the-moon-plus-more-fun-facts.jpg"
				alt="Forest" style="width: 100%">

		</div>
	</div>
	<br>
	<br>
	<center>
		<a href="#section1">

			<button class="button button2">BacktoInfo</button>
		</a>
	</center>
	<br>
	<br>
	<br>
	<center>
		<h2 id="section9" style="color: white; text-decoration-color: white;"
			class="line">O Negetive</h2>
		<br>
		<h5>
			<i>Your blood type is determined by genes inherited from your
				parents.Whether your blood type is rare, common or somewhere in
				between, your donations are vital in helping save and improve
				lives.People with O negative blood can only receive red cell
				donations from O negative donors.O negative blood is often called
				the â€˜universal blood typeâ€™ because people of any blood
				type can receive it.This makes it vitally important in an emergency
				or when a patientâ€™s blood type is unknown. </i>
		</h5>
	</center>

	<br>
	<center>
		<h5 style="color: white;" class="line">Donate And Recive</h5>
	</center>

	<img
		src="https://nhsbtdbe.blob.core.windows.net/umbraco-assets-corp/15069/29720-000np-know-your-type-web-buttons-400px-x-170px-blood-type-o.png"
		style="float: left;" width="300" height="130">
	<img
		src="https://nhsbtdbe.blob.core.windows.net/umbraco-assets-corp/15069/29720-000np-know-your-type-web-buttons-400px-x-170px-blood-type-o.png"
		style="float: right;" width="300" height="130">

	<center>
		<table>
			<tr>
				<th>Donate_to</th>
				<th>Recive_from</th>
			</tr>
			<tr>
				<td>ALL</td>
				<td>O-</td>
			</tr>
			<tr>
				<td>ALL</td>
				<td>-</td>
			</tr>
			<tr>
				<td>ALL</td>
				<td>-</td>
			</tr>
			<tr>
				<td>ALL</td>
				<td>-</td>
			</tr>

		</table>
	</center>
	<br>
	<center>
		<h5 style="color: white;" class="line">Facts</h5>
	</center>
	<br>
	<h5>
		<i>Around 13% of our blood donors have O negative blood.</i>
	</h5>
	<br>
	<h5>
		<i>O negative is the rarest of all types</i>
	</h5>
	<br>
	<h5>
		<i>People with type O blood are the least likely to suffer from
			cardiovascular issues.</i>
	</h5>
	<br>
	<h5>
		<i>People with type O blood are less susceptible to malaria.</i>
	</h5>
	<br>
	<center>
		<h5 style="color: white;" class="line">Diet has to followed</h5>
	</center>
	<img
		src="https://blogs.biomedcentral.com/on-medicine/wp-content/uploads/sites/6/2019/09/iStock-1131794876.t5d482e40.m800.xtDADj9SvTVFjzuNeGuNUUGY4tm5d6UGU5tkKM0s3iPk-620x342.jpg"
		style="float: right;" width="500" height="300">
	<br>
	<h5>
		<i>

			<ul>
				<li>Green vegetables</li>
				<li>meat (particularly lean meat and seafood for weight loss)</li>
				<li>eat lots of meat</li>
				<li>fish</li>
				<li>milk products</li>
				<li>seafood</li>
				<li>oat bran</li>
				<li>fruit but limit grains, beans, and legumes.</li>
				<li>oat meal and quinoa.</li>
				<li>olive oil</li>
				<li>broccoli, cauliflower, beets, cucumber</li>
			</ul> <br> <br>
		</i>
	</h5>

	<center>
		<h5 style="color: white;" class="line">Personality</h5>
	</center>
	<br>
	<h5>
		<i>Some of the positive personality traits in people with O are;
			leadership ability, self-determined, easygoing, optimistic, calm,
			confident, outgoing, loyal, cautious, passionate, peaceful,
			resilient, independent, trendsetter, reliable, carefree, and devoted.
			On the centrally, they are also jealous, rude, ruthless, insensitive,
			unpunctual, unpredictable, cold, self-centered, workaholic, and
			arrogant.Individuals with O are very enduring and strong, and that is
			why the Japanese call them Warriors. They are honest people and hate
			people who tell a lie or hide the truth. People who are O are not
			overly cautious about small details, as they tend to focus more on
			the big picture.</i>
	</h5>
	<br>
	<center>
		<h5 style="color: white;" class="line">Persons having O-ve blood
			group</h5>
	</center>
	<br>
	<h5>
		1.Cristiano Ronaldo <br> 2.A. R. Rahman<br>
	</h5>
	<br>
	<div class="row">
		<div class="column">
			<img
				src="https://pragativadi.com/wp-content/uploads/2021/01/cristiano-ronaldo-portugal_q5pbq2bgqojy1e0xdy8zl7tjb.jpg"
				alt="Snow" style="width: 100%">
		</div>
		<div class="column">
			<img
				src="https://cdn.dnaindia.com/sites/default/files/styles/full/public/2020/07/25/915354-ar-rahman-gang.jpg"
				alt="Forest" style="width: 100%">

		</div>
	</div>
	<br>
	<br>
	<center>
		<a href="#section1">

			<button class="button button2">BacktoInfo</button>
		</a>
	</center>
	<br>
	<br>
	<br>
</body>
</html>