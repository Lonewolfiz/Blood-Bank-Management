<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="en">
<head>

<title>Update Page</title>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1"> 
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet"
	href="https://fonts.googleapis.com/css?family=Lato">
<link rel="stylesheet"
	href="https://fonts.googleapis.com/css?family=Montserrat">
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css" />
	<link rel="stylesheet" type="text/css" href="/css/home.css">
<script type="text/javascript" src="/js/cities.js"></script>

</head>
<body>

	<div class="w3-bar w3-black w3-card w3-left-align w3-large">
		<a
			class="w3-bar-item w3-button w3-hide-medium w3-hide-large w3-left w3-padding-large w3-hover-white w3-large w3-black"
			href="javascript:void(0);" onclick="myFunction()"
			title="Toggle Navigation Menu"><i class="fa fa-bars"></i></a> <a
			href="/home"
			class="w3-bar-item w3-button w3-padding-large w3-hover-white"><i
			class="fa fa-home"></i> Home</a> <a href="/logout"
			class="w3-bar-item w3-button w3-hide-small w3-padding-large w3-hover-white w3-right"><i
			class="fa fa-sign-out" aria-hidden="true"></i> Sign-Out</a><a 
			class="w3-bar-item w3-button w3-hide-small w3-padding-large w3-white w3-right"><i
			class="fa fa-user-circle-o" aria-hidden="true"></i> Profile</a> <a href="/info"
			class="w3-bar-item w3-button w3-hide-small w3-padding-large w3-hover-white w3-right"><i
			class="fa fa-info-circle" aria-hidden="true"></i> Info</a>
	</div>

	
<div class="bg-image-login"></div>

	<div class="bg-text-about">
	<form:form action="/saveupdate" modelAttribute="uuser">
		<table align="center">
			<tr >
			
				<td >Name :</td>
				
				<td ><form:input path="name" /></td>
			</tr>
			<tr>
				
				<td><form:hidden path="username" /></td>
			</tr>
			<tr>
				<td>Mobile :</td>
				<td><form:input path="mobile" /></td>
			</tr>
			<tr>
				<td>Blood :</td>
				<td><form:input path="blood" /></td>
			</tr>
			<tr>
				<td>State :</td>
				<td><form:input path="state" /></td>
				
			</tr>
			<tr>
				<td>City :</td>
				<td><form:input path="city" /></td>
			</tr>
			<tr>
				<td>Gender :</td>
				<td><form:input path="gender" /></td>
			</tr>
			<tr>
				<td>Date of Birth:</td>
				<td><form:input type="date" path="dateOfBirth" /></td>
			</tr>
			<tr>
				<td>Date of Last Donation:</td>
				<td><form:input type="date" path="dateOfLastDonation" /></td>
			</tr>
			<tr>
					<td colspan="2">
						<center>
							<input type="submit" value="Update"
								class="w3-button w3-black w3-round-large w3-hover-white">
						</center>
					</td>
				</tr>
           
			<th  colspan="2">
                   <h1 style="color:white"> ${message}</h1>
                   </th>
		</table>
	
	</form:form>
	</div>
</body>
</html>