<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet"
	href="https://fonts.googleapis.com/css?family=Lato">
<link rel="stylesheet"
	href="https://fonts.googleapis.com/css?family=Montserrat">
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css" />
<link rel="stylesheet" type="text/css" href="/css/bgstyle.css">
<link rel="stylesheet" type="text/css" href="/css/home.css">
<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css"
	rel="stylesheet"
	integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x"
	crossorigin="anonymous">
<script
	src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js"
	integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4"
	crossorigin="anonymous"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script>
$(document).ready(function(){
	  $("#idea").click(function(){
	   alert("Notified successfully!!!");
	  });
	});
</script>
</head>
<body>

	<div class="w3-bar w3-black w3-card w3-left-align w3-large">
		<a
			class="w3-bar-item w3-button w3-hide-medium w3-hide-large w3-left w3-padding-large w3-hover-white w3-large w3-black"
			href="javascript:void(0);" onclick="myFunction()"
			title="Toggle Navigation Menu"><i class="fa fa-bars"></i></a> <a
			href="/home"
			class="w3-bar-item w3-button w3-padding-large w3-hover-white"><i
			class="fa fa-home"></i> Home</a> <a href="/logout"
			class="w3-bar-item w3-button w3-hide-small w3-padding-large w3-hover-white w3-right"><i
			class="fa fa-sign-out" aria-hidden="true"></i> LOGOUT</a> <a
			href="/getByBloodGroup"
			class="w3-bar-item w3-button w3-hide-small w3-padding-large w3-hover-white w3-right"><i
			class="fa fa-arrow-left" aria-hidden="true"></i> BACK</a>
	</div>





	<div class="container">
		<h2 align="center">Donor Details</h2>
		<table border="1" align="center"
			class="table table-hover table-bordered table-dark">
			<tr>
				<td><b>Name</b></td>
				<td><b>Mail</b></td>
				<td><b>Mobile</b></td>
				<td><b>State</b></td>
				<td><b>City</b></td>
				<td><b>Gender</b></td>
				<td><b>Actions</b></td>



			</tr>
			<c:forEach items="${user}" var="user">
				<tr>
					<td><c:out value="${user.name}"></c:out></td>
					<td><c:out value="${user.username}"></c:out></td>
					<td><c:out value="${user.mobile}"></c:out></td>

					<td><c:out value="${user.state}"></c:out></td>
					<td><c:out value="${user.city}"></c:out></td>
					<td><c:out value="${user.gender}"></c:out></td>
					<td><a href="/send-notify-message?name=${user.username}"  id="idea">notify</a></td>


</script>
				</tr>
			</c:forEach>
		</table>
		
		<h1> ${success}</h1>
		<a class="btn btn-success" href="/suggestion">Suggestion</a>
		<%-- ${msg} --%>
		

	</div>
</body>
</html>